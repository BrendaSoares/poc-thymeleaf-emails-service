package com.southsystem.pocemail.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

public class Mail {

    private String from;
    private String[] mailTo;
    private String subject;
    private List<Object> attachments;
    private Map<String, Object> props;
    private String eventType;

    public Mail() {
    }

    public Mail(String from, String[] mailTo, String subject, List<Object> attachments, Map<String, Object> props, String eventType) {
        this.from = from;
        this.mailTo = mailTo;
        this.subject = subject;
        this.attachments = attachments;
        this.props = props;
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String[] getMailTo() {
        return mailTo;
    }

    public void setMailTo(String[] mailTo) {
        this.mailTo = mailTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<Object> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Object> attachments) {
        this.attachments = attachments;
    }

    public Map<String, Object> getProps() {
        return props;
    }

    public void setProps(Map<String, Object> props) {
        this.props = props;
    }
}
