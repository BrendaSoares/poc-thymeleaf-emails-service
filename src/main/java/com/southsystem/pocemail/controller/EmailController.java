package com.southsystem.pocemail.controller;

import com.southsystem.pocemail.model.Mail;
import com.southsystem.pocemail.service.EmailSenderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/email")
public class EmailController {

    private final EmailSenderService service;

    public EmailController(EmailSenderService service) {
        this.service = service;
    }

    @PostMapping
    @ApiOperation(value = "Send email", response = Mail.class)
    public void sendEmail(@RequestBody Mail mail) throws MessagingException {
        service.sendEmail(mail);
    }
}
