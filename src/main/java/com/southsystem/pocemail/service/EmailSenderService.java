package com.southsystem.pocemail.service;

import com.southsystem.pocemail.model.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailSenderService {

    @Autowired
    private final JavaMailSender emailSender;

    @Qualifier("templateEngine")
    @Autowired
    private SpringTemplateEngine templateEngine;

    public EmailSenderService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public void sendEmail(Mail mail) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        helper.addAttachment("impulsione.png", new ClassPathResource("southsystem.jpg"));
        helper.setTo(mail.getMailTo());
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());
        Map<String, Object> model = new HashMap<>();

        model.put("name", "Developer!");
        model.put("location", "United States");
        model.put("sign", "Java Developer");
        mail.setProps(model);

        Context context = new Context();
        context.setVariables(mail.getProps());

        String html = templateEngine.process(mail.getEventType(), context);
        helper.setText(html, true);

        this.emailSender.send(message);
    }
}
