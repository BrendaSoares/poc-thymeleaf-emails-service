package com.southsystem.pocemail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobiiEmailServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(com.southsystem.pocemail.JobiiEmailServiceApplication.class, args);
	}

}
